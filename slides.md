% GitLab Pages
% Tanuki
% Today

# In the morning

- Writing markdown
- Drink coffee

## During the day

- Converting slides with pandoc

# In the evening

- Presenting slides

# Conclusion

- And the answer is...
- $f(x)=\sum_{n=0}^\infty\frac{f^{(n)}(a)}{n!}(x-a)^n$    
